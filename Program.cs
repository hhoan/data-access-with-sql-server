﻿using DataAccessWithSQLServer.Models;
using DataAccessWithSQLServer.Repositories;

namespace DataAccessWithSQLServer
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepository repository = new CustomerRepository();
            //TestSelectAll(repository);
            //TestSelectById(repository, 2);
            //TestSelectByName(repository, "Astrid");
            //TestSelectPage(repository, 5, 5);
            //TestAdd(repository);
            //TestUpdate(repository);
            //TestSelectCustomersByCountries(repository);
            //TestSelectCustomersBySpending(repository);
            TestSelectCustomerGenre(repository);
        }
        #region TESTS CustomerRepository
        static void TestSelectAll(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAllCustomers());
        }

        static void TestSelectById(ICustomerRepository repository, int id)
        {
            PrintCustomer(repository.GetCustomer(id));
        }

        static void TestSelectByName(ICustomerRepository repository, string name)
        {
            PrintCustomers(repository.GetCustomerByName(name));
        }

        static void TestSelectPage(ICustomerRepository repository, int limit, int offset)
        {
            PrintCustomers(repository.GetCustomersPage(limit, offset));
        }

        static void TestAdd(ICustomerRepository repository)
        {
            Customer test = new Customer()
            {
                FirstName = "Jane",
                LastName = "Doe",
                Country = "Germany",
                PostalCode = "12345",
                Phone = "123456789",
                Email = "JaneDoe@email.com"
            };
            if (repository.AddCustomer(test))
            {
                Console.WriteLine("Customer successfully added.");
                TestSelectByName(repository, test.FirstName);
            }
            else Console.WriteLine("Could not add customer.");
        }

        static void TestUpdate(ICustomerRepository repository)
        {
            Customer test = new Customer()
            {
                CustomerId = 61,
                FirstName = "Banana",
                LastName = "Doe",
                Country = "Germany",
                PostalCode = "12345",
                Phone = "123456789",
                Email = "BananaDoe@email.com"
            };
            if (repository.UpdateCustomer(test))
            {
                Console.WriteLine("Customer successfully updated.");
                TestSelectById(repository, test.CustomerId);
            }
            else Console.WriteLine("Could not update customer.");
        }

        static void TestSelectCustomersByCountries(ICustomerRepository repository)
        {
            List<CustomerCountry> test = repository.GetCustomersByCountry();
            foreach (var country in test)
            {
                Console.WriteLine($"--- {country.Country} {country.Number}");
            }
        }
        static void TestSelectCustomersBySpending(ICustomerRepository repository)
        {
            List<CustomerSpender> test = repository.GetCustomersBySpending();
            foreach (var spender in test)
            {
                Console.WriteLine($"--- {spender.CustomerId} {spender.CustomerName} {spender.Total}$");
            }
        }

        static void TestSelectCustomerGenre(ICustomerRepository repository)
        {
            List<CustomerGenre> test = repository.GetCustomerGenre(12);
            foreach (var genre in test)
            {
                Console.WriteLine($"--- {genre.GenreName} {genre.TotalPurchases}");
            }
        }

        static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (var customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"--- {customer.CustomerId} {customer.FirstName} {customer.LastName} {customer.Country} {customer.PostalCode} {customer.Phone} {customer.Email}");
        }
        #endregion
    }
}