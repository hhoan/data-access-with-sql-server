--Populate Assistant table

USE SuperheroesDb

INSERT INTO Assistant (Name, SuperheroId) 
	VALUES ('Alfred Pennyworth', 1);
INSERT INTO Assistant (Name, SuperheroId) 
	VALUES ('Pietro Maximoff', 2);
INSERT INTO Assistant (Name, SuperheroId) 
	VALUES ('Mary Jane Watson', 3);
