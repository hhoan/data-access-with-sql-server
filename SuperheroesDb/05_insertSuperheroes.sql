--Populate Superhero table

USE SuperheroesDb

INSERT INTO Superhero (Name, Alias, Origin) 
	VALUES ('Bruce Wayne', 'Batman', 'Gotham');

INSERT INTO Superhero (Name, Alias, Origin) 
	VALUES ('Wanda Maximoff', 'Scarlet Witch', 'Sokovia');

INSERT INTO Superhero (Name, Alias, Origin) 
	VALUES ('Peter Parker', 'Spider-Man', 'Queens');