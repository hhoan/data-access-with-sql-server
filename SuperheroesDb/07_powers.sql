USE SuperheroesDb

--Add powers
INSERT INTO [dbo].[Power] (Name, Description) 
	VALUES ('Money', 'Just being rich');
INSERT INTO [dbo].[Power] (Name, Description) 
	VALUES ('Spider-Sense', 'Warns of incoming danger');
INSERT INTO [dbo].[Power] (Name, Description) 
	VALUES ('Neuroelectric interfacing', 'Read thoughts and give targets waking nightmares');
INSERT INTO [dbo].[Power] (Name, Description) 
	VALUES ('BigBrain', 'Super high intellect which gives and advantage in combat');

--Add powers to superheroes
INSERT INTO SuperheroPower
	VALUES(1, 1);
INSERT INTO SuperheroPower
	VALUES(2, 3);
INSERT INTO SuperheroPower
	VALUES(3, 2);
INSERT INTO SuperheroPower
	VALUES(1, 4);
INSERT INTO SuperheroPower
	VALUES(3, 4);