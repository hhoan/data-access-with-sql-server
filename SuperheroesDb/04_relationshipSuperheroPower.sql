USE SuperheroesDb;

--Create linking table 
CREATE TABLE SuperheroPower(
    SuperheroId INT NOT NULL,
    PowerId INT NOT NULL,
    PRIMARY KEY (SuperheroId, PowerId)
);

ALTER TABLE SuperheroPower
    ADD CONSTRAINT FK_SuperheroPower_Superhero FOREIGN KEY (SuperheroId) REFERENCES Superhero(Id);
ALTER TABLE SuperheroPower
    ADD CONSTRAINT FK_SuperherPower_Power FOREIGN KEY (PowerId) REFERENCES Power(Id);
