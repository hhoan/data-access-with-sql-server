--Add foreign keys to tables

USE SuperheroesDb;

ALTER TABLE Assistant
	ADD superheroId INT;

ALTER TABLE Assistant
	ADD CONSTRAINT FK_Assistant_Superhero FOREIGN KEY (superheroId) REFERENCES Superhero(Id);
