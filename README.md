# Data Access With SQL Server

This repository contains two parts of a project for accessing data with SQL Server.

## Description

[SuperheroesDb](https://gitlab.com/hhoan/data-access-with-sql-server/-/tree/main/SuperheroesDb) - Contains scripts to create a DB with Superheroes, Assistants and Power, and with relationships between the tables and some dummy data filled in.

The other part of the project is for accessing and manipulating data in 'Chinook' database with SQLClient and the repository pattern. Chinook is a database that models the iTunes database of customers purchasing tracks. The project is a Console Application using C# and EntityFrameworkCore.

## Install

Download the repository or use git to clone the repository:

```
git clone git@gitlab.com:hhoan/data-access-with-sql-server.git
```

## Usage
After downloading the repository you can run the project locally. 

Run the SQL scripts in SuperheroesDd in SQL Server Management Studio.

## Contributors

[Ha Hoang](https://gitlab.com/hhoan)

