﻿using DataAccessWithSQLServer.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessWithSQLServer.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Gets data from DB and stores it in a customer List.
        /// </summary>
        /// <returns>Customer List</returns>
        public List<Customer> GetAllCustomers()
        {
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerID, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            try
            {
                // Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Reader
                            while (reader.Read())
                            {
                                // Handle result
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.IsDBNull(3) ? null: reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? null : reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                customerList.Add(temp);
                            }
                        }
                    }               
                }
            }
            catch (SqlException ex)
            {
                // Log error
                Console.WriteLine(ex.Message);
            }

            return customerList;
        }
        
        /// <summary>
        /// Gets customer data by id from DB and stores it 
        /// </summary>
        /// <param name="id">CustomerId</param>
        /// <returns>Customer</returns>
        public Customer GetCustomer(int id)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerID, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerID = " + id;
            try
            {
                // Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Reader
                            while (reader.Read())
                            {
                                // Handle result
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.IsDBNull(3) ? null : reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                customer.Phone = reader.IsDBNull(5) ? null : reader.GetString(5);
                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log error
                Console.WriteLine(ex.Message);
            }

            return customer;
        }

        /// <summary>
        /// Get customer data by name from DB and store it
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Customer data</returns>
        public List<Customer> GetCustomerByName(string name)
        {
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerID, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CONCAT(FirstName, ' ', LastName) LIKE '%' + @name + '%'";
            try
            {
                // Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@name", name);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Reader
                            while (reader.Read())
                            {
                                // Handle result
                                customerList.Add(new Customer
                                {
                                    CustomerId = reader.GetInt32(0),
                                    FirstName = reader.GetString(1),
                                    LastName = reader.GetString(2),
                                    Country = reader.IsDBNull(3) ? null : reader.GetString(3),
                                    PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4),
                                    Phone = reader.IsDBNull(5) ? null : reader.GetString(5),
                                    Email = reader.GetString(6)

                                });
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log error
                Console.WriteLine(ex.Message);
            }

            return customerList;
        }

        /// <summary>
        /// Get subset of customers data from DB and store it in a list.
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns>Customer list</returns>
        public List<Customer> GetCustomersPage(int limit, int offset)
        {
            List<Customer> customerList = new List<Customer>();
            string sql = $"SELECT CustomerID, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer ORDER BY CustomerID OFFSET {offset} ROWS FETCH NEXT {limit} ROWS ONLY";
            try
            {
                // Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Reader
                            while (reader.Read())
                            {
                                // Handle result
                                customerList.Add(new Customer
                                {
                                    CustomerId = reader.GetInt32(0),
                                    FirstName = reader.GetString(1),
                                    LastName = reader.GetString(2),
                                    Country = reader.IsDBNull(3) ? null : reader.GetString(3),
                                    PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4),
                                    Phone = reader.IsDBNull(5) ? null : reader.GetString(5),
                                    Email = reader.GetString(6)

                                });
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log error
                Console.WriteLine(ex.Message);
            }

            return customerList;
        }

        /// <summary>
        /// Insert new customer into DB table.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>true or false</returns>
        public bool AddCustomer(Customer customer)
        {
            bool success = false;
            string sql = "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email) VALUES(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                // Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return success;
        }


        /// <summary>
        /// Update existing customer in DB
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>true or false</returns>
        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            string sql = "UPDATE Customer SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email WHERE CustomerId = @CustomerId";
            try
            {
                // Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return success;
        }

        /// <summary>
        /// Get numbers of customers in each country in descending order and store in customerCountryList.
        /// </summary>
        /// <returns>customerCountryList</returns>
        public List<CustomerCountry> GetCustomersByCountry()
        {
            List<CustomerCountry> customerCountryList = new List<CustomerCountry>();
            string sql = "SELECT Country,COUNT(CustomerId) FROM Customer GROUP BY Country ORDER BY COUNT(CustomerId) DESC";
            try
            {
                // Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Reader
                            while (reader.Read())
                            {
                                // Handle result
                                customerCountryList.Add(new CustomerCountry
                                {
                                    Country = reader.GetString(0),
                                    Number = reader.GetInt32(1)
                                });
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log error
                Console.WriteLine(ex.Message);
            }

            return customerCountryList;
        }

        /// <summary>
        /// Get highest spending customers in descending order and store in customerSpenderList.
        /// </summary>
        /// <returns>customerSpenderList</returns>
        public List<CustomerSpender> GetCustomersBySpending()
        {
            List<CustomerSpender> customerSpenderList = new List<CustomerSpender>();
            string sql = "SELECT Customer.CustomerId, CONCAT(FirstName, ' ', LastName) AS CustomerName, SUM(Invoice.Total) AS Total FROM Invoice JOIN Customer ON Invoice.CustomerId = Customer.CustomerId GROUP BY Customer.CustomerId, Customer.FirstName, Customer.LastName ORDER BY Total DESC";
            try
            {
                // Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Reader
                            while (reader.Read())
                            {
                                // Handle result
                                customerSpenderList.Add(new CustomerSpender
                                {
                                    CustomerId = reader.GetInt32(0),
                                    CustomerName = reader.GetString(1),
                                    Total = (double)reader.GetDecimal(2)
                                });
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log error
                Console.WriteLine(ex.Message);
            }

            return customerSpenderList;
        }

        /// <summary>
        /// Get top 1 popular genre with tie for a customer by id and store in customerGenreList.
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns>customerGenreList</returns>
        public List<CustomerGenre> GetCustomerGenre(int customerId)
        {
            List<CustomerGenre> customerGenreList = new List<CustomerGenre>();
            string sql = "SELECT TOP 1 WITH TIES Genre.Name AS Genre, COUNT(*) AS TotalPurchases FROM Genre "
                + "INNER JOIN Track ON Track.GenreId = Genre.GenreId "
                + "INNER JOIN InvoiceLine ON InvoiceLine.TrackId = Track.TrackId "
                + "INNER JOIN Invoice ON Invoice.InvoiceId = InvoiceLine.InvoiceId "
                + "WHERE Invoice.CustomerId = @CustomerId "
                + "GROUP BY Genre.Name ORDER BY TotalPurchases DESC;";
            try
            {
                // Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customerId);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Reader
                            while (reader.Read())
                            {
                                // Handle result
                                customerGenreList.Add(new CustomerGenre
                                {
                                    GenreName = reader.GetString(0),
                                    TotalPurchases = reader.GetInt32(1)
                                });
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log error
                Console.WriteLine(ex.Message);
            }

            return customerGenreList;
        }
    }
}
