﻿using DataAccessWithSQLServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessWithSQLServer.Repositories
{
    public interface ICustomerRepository
    {
        public List<Customer> GetAllCustomers();
        public Customer GetCustomer(int id);
        public List<Customer> GetCustomerByName(string name);
        public List<Customer> GetCustomersPage(int limit, int offset);
        public bool AddCustomer(Customer customer);
        public bool UpdateCustomer(Customer customer);
        public List<CustomerCountry> GetCustomersByCountry();
        public List<CustomerSpender> GetCustomersBySpending();
        public List<CustomerGenre> GetCustomerGenre(int customerId);  
    }
}
