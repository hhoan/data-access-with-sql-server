﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessWithSQLServer.Models
{
    public class CustomerSpender
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; } = string.Empty;
        public double Total { get; set; }
    }
}
