﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessWithSQLServer.Models
{
    public class CustomerGenre
    {
        public int CustomerId { get; set; }
        public string GenreName { get; set; } = null!;
        public int TotalPurchases { get; set; }
    }
}
